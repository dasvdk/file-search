"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const path = require("path");
class FileTask {
    constructor(paths, mask, timeout = null) {
        this.paths = paths;
        this.abort = false;
        this.mask = new RegExp("^" + mask.replace(".", "\\.").replace("-", "\\-").replace("*", ".*?") + "$");
        if (timeout !== null)
            setTimeout(() => this.abort = true, timeout);
    }
    runOnce() {
        return __awaiter(this, void 0, void 0, function* () {
            let p = this.paths.shift();
            if (typeof p === "undefined" || p === null) {
                return null;
            }
            if (p.isdir) {
                let contents;
                try {
                    contents = yield fs.promises.readdir(p.fullname);
                }
                catch (e) {
                    return null;
                }
                for (let newPath of contents) {
                    let fullname = path.join(p.fullname, newPath);
                    let stats;
                    try {
                        stats = yield fs.promises.stat(fullname);
                    }
                    catch (e) {
                        continue;
                    }
                    let newFileInfo = { name: newPath, fullname: fullname, isdir: stats.isDirectory() };
                    if (!newFileInfo.isdir) // investigate files first
                        this.paths.unshift(newFileInfo);
                    else
                        this.paths.push(newFileInfo);
                }
            }
            // only return non-null if the fileinfo is a file and matches the mask
            return p.isdir || !this.mask.test(p.name) ? null : p;
        });
    }
    run(report, reportDir = null) {
        return __awaiter(this, void 0, void 0, function* () {
            while (!this.abort) {
                let res = yield this.runOnce();
                if (res !== null)
                    report(res);
            }
        });
    }
}
exports.FileTask = FileTask;
//# sourceMappingURL=main.js.map