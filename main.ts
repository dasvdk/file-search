export type FileInfo = { name: string, fullname: string, isdir: boolean }

import * as fs from 'fs'
import * as path from 'path'

export class FileTask {
    paths: FileInfo[]
    abort: boolean
    mask: RegExp
    constructor(paths: FileInfo[], mask: string, timeout: number | null = null) {
        this.paths = paths
        this.abort = false
        this.mask = new RegExp("^" + mask.replace(".", "\\.").replace("-","\\-").replace("*", ".*?") + "$")
        if (timeout !== null) setTimeout(() => this.abort = true, timeout)
    }

    async runOnce() : Promise<FileInfo> {
        let p = this.paths.shift()
        if (typeof p === "undefined" || p === null) {
            return null
        }
        
        if (p.isdir) {
            let contents: string[]
            try {
                contents = await fs.promises.readdir(p.fullname)
            } catch (e) {
                 return null 
            }
            for (let newPath of contents) {
                let fullname = path.join(p.fullname, newPath)
                let stats: fs.Stats
                try {
                    stats = await fs.promises.stat(fullname)
                } catch (e) { continue; }
                
                let newFileInfo = { name: newPath, fullname: fullname, isdir: stats.isDirectory() }
                if (!newFileInfo.isdir) // investigate files first
                    this.paths.unshift(newFileInfo) 
                else
                    this.paths.push(newFileInfo)
            }
        } 

        // only return non-null if the fileinfo is a file and matches the mask
        return p.isdir || !this.mask.test(p.name) ? null : p
    }

    async run(report: (fs: FileInfo) => void, reportDir: (fs: FileInfo) => void = null) : Promise<void> {
        while (!this.abort) {
            let res = await this.runOnce()
            if (res !== null)
                report(res)
        }
    }
}