export declare type FileInfo = {
    name: string;
    fullname: string;
    isdir: boolean;
};
export declare class FileTask {
    paths: FileInfo[];
    abort: boolean;
    mask: RegExp;
    constructor(paths: FileInfo[], mask: string, timeout?: number | null);
    runOnce(): Promise<FileInfo>;
    run(report: (fs: FileInfo) => void, reportDir?: (fs: FileInfo) => void): Promise<void>;
}
